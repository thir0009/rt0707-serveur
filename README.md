# RT0707 - Serveur

Démarrez le docker Rabbitmq **cd AMQP** **sudo docker-compose up**.
Le docker de l'API REST **Web_Serveur/API sudo docker-compose build** **sudo docker-compose up** et de meme pour **Web_Serveur/Web**.
Démarrez les deux script **python3 Subscriber/subscriber_mqtt.py** **python3 Subscriber/subscriber_amqp.py**.
Démarrez la simulation de l'app android **cd Livreur/demo; mvn exec:java**.
